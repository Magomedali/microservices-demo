variable "aws_region" {
  description = "Region"
  type = string
}

variable "env" {
  description = "Environment. stand or prod"
  type = string
  default = "stand"
}

variable "md_region" {
  description = "md_region"
  type = string
}

variable "k8s_version" {
  description = "Version of k8s"
  type = string
  default = "1.19"
}

variable "shared_credentials_file" {
  description = "shared credentials file"
  type = string
  sensitive = true
}

variable "ami_account_id" {
  description = "Amazon EKS AMI Account ID"
  type = string
  sensitive = true
}

variable "profile" {
  description = "profile"
  type = string
  sensitive = true
}

variable "k8s_master_whitelist" {
  description = "IP whitelist"
  type = list(string)
  default = [
    "192.168.0.0/24",
    "85.23.23.22/32"
  ]
}

variable "office_external_ips" {
  description = "Office external ip addresses"
  type = list(string)
  default = ["196.92.178.51/32", "76.124.28.112/32"]
}

variable "aws_ami_name" {
  description = "Amazon machine image name"
  type = string
}

variable "aws_ami_type" {
  description = "Amazon machine type"
  type = string
}

variable "node_instance_type" {
  description = "Node instance type"
  type = string
  default = "m5.xlarge"
}

variable "k8s_node_min_count" {
  description = "Min node count for auto scale"
  type = number
  default = 3
}

variable "k8s_node_max_count" {
  description = "Max node count for auto scale"
  type = number
  default = 10
}

variable "ssh_key_pair_name" {
  description = "Ssh key name for nodes"
  type = string
  default = "Ali"
}