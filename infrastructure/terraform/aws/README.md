Создание кластера в AWS
------------------------------

Инициализировать провайдера:
```shell
> terraform init
```

Выбрать workspace с которым будете работать(development или production):
```shell
> terraform workspace select eu-stand
```

Сконфигурировать параметры кластера в файле - **./terraform.tfstate.d/\<workspace\>/secret.tfvars**

Проверить вносимые изменения:
```shell
> terraform plan --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

Применить изменения
```shell
> terraform apply --var-file=terraform.tfstate.d/<workspace>/secret.tfvars
```

Terraform создаст все необходимое для кластера, возможно, кроме присоединения виртуалок к кластеру

Присоединение виртуалок к кластеру, если их нет в кластере :

```shell
> terraform output config_map_aws_auth
```
Сохранить config_map_aws_auth в файл **config_map_aws_auth.yaml**

Настроить kubeconfig
```shell
> aws eks update-kubeconfig --name <cluster_name> --alias <context_name> --region <region>
```

Добавить config_map_aws_auth в k8s:

```shell
> kubectl apply -f config_map_aws_auth.yaml
```

Проверить присоединение виртуалок к k8s
```shell
> kubectl get nodes --watch
```

Для работы с k8s из наших серверов и для cicd создан профиль ci_user с необходимыми правами. Надо дать разрешить этому профилю доступ к новому кластеру

Для этого надо отредактировать configmap auth_aws, добавить следующее:

```yaml
...
 mapUsers: |
   - userarn: arn:aws:iam::<acc_id>:user/ci_user
     usename: ci_user
     groups:
       - system:masters
...
```

