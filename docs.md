
Создание ssh ключа:
```shell
> flux create secret git gitlab-auth --url ssh://git@gitlab.com/Magomedali/microservices-demo --ssh-key-algorithm=ecdsa --ssh-ecdsa-curve=p521
```

Установка fluxcd
```shell
> flux bootstrap gitlab --owner=Magomedali --repository=microservices-demo --branch=master --path=clusters/prod --ssh-hostname=gitlab.com --secret-name gitlab-auth --components-extra=image-reflector-controller,image-automation-controller
```

Директория в репозитории за которую следит flux: clusters/prod


Установка istio
```shell
> istioctl install --set profile=default

> kubectl -n istio-system apply -f istio/samples/addons/prometheus.yaml
> kubectl -n istio-system apply -f istio/samples/addons/grafana.yaml
> kubectl -n istio-system apply -f istio/samples/addons/kiali.yaml
```

Установка flagger
```shell
> kubectl apply -f https://raw.githubusercontent.com/fluxcd/flagger/main/artifacts/flagger/crd.yaml

> helm upgrade -i flagger flagger/flagger \
--namespace=istio-system \
--set crd.create=false \
--set meshProvider=istio \
--set metricsServer=http://prometheus:9090
```

Схема трафика в kiali 
![img.png](img.png)

Схема
![img_1.png](img_1.png)

Схема трафика при canary деплойменте(старый под и новый подс с новой версией frontend)
![img_2.png](img_2.png)
После успешно релиза старый под удалился
![img_3.png](img_3.png)

Завершение релиза:

Статус релиза:
```shell
MacBook-Pro-Ali-2:microservices-demo ali$ kubectl get canaries -n microservices-demo
NAME       STATUS      WEIGHT   LASTTRANSITIONTIME
frontend   Succeeded   0        2022-08-09T21:10:16Z
```

[описание релиза](./canary_deployment.report.yaml)
```yaml
Name:         frontend
Namespace:    microservices-demo
Labels:       app.kubernetes.io/managed-by=Helm
              helm.toolkit.fluxcd.io/name=frontend
              helm.toolkit.fluxcd.io/namespace=microservices-demo
Annotations:  meta.helm.sh/release-name: frontend
              meta.helm.sh/release-namespace: microservices-demo
API Version:  flagger.app/v1beta1
Kind:         Canary
Metadata:
  Creation Timestamp:  2022-08-09T20:39:13Z
  Generation:          1
  Managed Fields:
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:meta.helm.sh/release-name:
          f:meta.helm.sh/release-namespace:
        f:labels:
          .:
          f:app.kubernetes.io/managed-by:
          f:helm.toolkit.fluxcd.io/name:
          f:helm.toolkit.fluxcd.io/namespace:
      f:spec:
        .:
        f:analysis:
          .:
          f:interval:
          f:maxWeight:
          f:stepWeight:
          f:threshold:
        f:provider:
        f:service:
          .:
          f:gateways:
          f:hosts:
          f:port:
          f:targetPort:
          f:trafficPolicy:
            .:
            f:tls:
              .:
              f:mode:
        f:targetRef:
          .:
          f:apiVersion:
          f:kind:
          f:name:
    Manager:      helm-controller
    Operation:    Update
    Time:         2022-08-09T20:39:13Z
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:status:
        .:
        f:canaryWeight:
        f:conditions:
        f:failedChecks:
        f:iterations:
        f:lastAppliedSpec:
        f:lastPromotedSpec:
        f:lastTransitionTime:
        f:phase:
        f:trackedConfigs:
    Manager:         flagger
    Operation:       Update
    Subresource:     status
    Time:            2022-08-09T20:40:17Z
  Resource Version:  731147
  UID:               d9fdb098-b3f5-4828-8b9c-54eda317a075
Spec:
  Analysis:
    Interval:     1m
    Max Weight:   50
    Step Weight:  5
    Threshold:    10
  Provider:       istio
  Service:
    Gateways:
      frontend
    Hosts:
      *
    Port:         80
    Target Port:  8080
    Traffic Policy:
      Tls:
        Mode:  DISABLE
  Target Ref:
    API Version:  apps/v1
    Kind:         Deployment
    Name:         frontend
Status:
  Canary Weight:  0
  Conditions:
    Last Transition Time:  2022-08-09T21:10:16Z
    Last Update Time:      2022-08-09T21:10:16Z
    Message:               Canary analysis completed successfully, promotion finished.
    Reason:                Succeeded
    Status:                True
    Type:                  Promoted
  Failed Checks:           0
  Iterations:              0
  Last Applied Spec:       84949c6d
  Last Promoted Spec:      84949c6d
  Last Transition Time:    2022-08-09T21:10:16Z
  Phase:                   Succeeded
  Tracked Configs:
Events:
  Type     Reason  Age                From     Message
  ----     ------  ----               ----     -------
  Warning  Synced  49m                flagger  frontend-primary.microservices-demo not ready: waiting for rollout to finish: observed deployment generation less than desired generation
  Normal   Synced  48m (x2 over 49m)  flagger  all the metrics providers are available!
  Normal   Synced  48m                flagger  Initialization done! frontend.microservices-demo
  Normal   Synced  31m                flagger  New revision detected! Scaling up frontend.microservices-demo
  Normal   Synced  30m                flagger  Starting canary analysis for frontend.microservices-demo
  Normal   Synced  30m                flagger  Advance frontend.microservices-demo canary weight 5
  Normal   Synced  29m                flagger  Advance frontend.microservices-demo canary weight 10
  Normal   Synced  28m                flagger  Advance frontend.microservices-demo canary weight 15
  Normal   Synced  27m                flagger  Advance frontend.microservices-demo canary weight 20
  Normal   Synced  26m                flagger  Advance frontend.microservices-demo canary weight 25
  Normal   Synced  25m                flagger  Advance frontend.microservices-demo canary weight 30
  Normal   Synced  24m                flagger  Advance frontend.microservices-demo canary weight 35
  Normal   Synced  18m (x6 over 23m)  flagger  (combined from similar events): Promotion completed! Scaling down frontend.microservices-demo

```